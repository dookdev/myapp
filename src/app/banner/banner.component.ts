import { Component, OnInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.scss']
})
export class BannerComponent implements OnInit, OnChanges {
  @Input() title: any;
  @Output() respData = new EventEmitter();

  constructor() {

  }

  ngOnInit(): void {

  }

  ngOnChanges(): void {
    console.log(this.title);
  }

  onClickResponse(): void {
    this.respData.emit({ tel: '09877676767', address: 'bkk' });
  }

}
