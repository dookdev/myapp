import { Component } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'myapp';
  data: any = {
    name: 'dook',
    email: 'dook@gmail.com'
  };
  constructor(private spinner: NgxSpinnerService) {
    // this.runSpinner();
  }

  runSpinner(): void {
    console.log(this.title);
    console.log(this.data.email);
    this.spinner.show();
  }

  onResponse(e: any): void {
    console.log(e);
  }
}
